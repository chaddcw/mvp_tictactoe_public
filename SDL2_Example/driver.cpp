//****************************************************************************
// File name:	 driver.cpp
// Author:     Chadd Williams
// Date:       4/12/2019
// Class:      CS 485
// Assignment: MVP
// Purpose:    Demonstrate how to use SDL widgets and events.
//****************************************************************************

#include "SDL2ExampleApp.h"

//***************************************************************************
// Function:    main
//
// Description: Create the SDLApp window and launch game loop
//
// Parameters:  argc - number of command line arguments
//              argv - the command line arguments
//
// Returned:    int - EXIT_SUCCESS
//***************************************************************************
int main (int argc, char *argv[])
{
  SDL2ExampleApp cTheDisplay;


  cTheDisplay.createWindow ("SDL Example");
  cTheDisplay.setWindowBackgroundColor ({ 255, 255, 255, 255 });

  cTheDisplay.gameLoop ();

  cTheDisplay.cleanup ();

  return EXIT_SUCCESS;
}