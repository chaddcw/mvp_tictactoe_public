//****************************************************************************
// File name:	 SDL2ExampleApp.cpp
// Author:     Chadd Williams
// Date:       4/12/2019
// Class:      CS 485
// Assignment: MVP
// Purpose:    Demonstrate how to use SDL widgets and events.
//****************************************************************************

#include "SDL2ExampleApp.h"


//***************************************************************************
// Constructor: SDL2ExampleApp
//
// Description: Initialize App
//
// Parameters:  None
//
// Returned:    None
//***************************************************************************
SDL2ExampleApp::SDL2ExampleApp ()
{

  // Load fonts
  mConsolasFontID = loadFont ("c:/windows/Fonts/consola.ttf", 20);
  mVerdanaFontID = loadFont ("c:/windows/Fonts/verdana.ttf", 20);

  // create a non-editable text widget
  mpcNotEditableText = new SDLTextWidget ("Non-Editable", "Hi", 10, 100,
    mConsolasFontID, { 0,0,0,255 });

  // set the widget to non-editable
  mpcNotEditableText->setEditable (false);


  // create an editable text widget
  mpcEditableText = new SDLTextWidget ("Editable", "Type Here", 10, 200,
    mVerdanaFontID, { 0,0,0,255 });

  // set the widget to editable
  mpcEditableText->setEditable (true);

  // register a state change handler
  mpcEditableText->registerStateChangeEventHandler
  (std::bind (&SDL2ExampleApp::editableTextBoxStateChangeHandler, this));

  // register the text widget with SDLApp so it will handle
  // events correctly
  registerTextWidget (mpcEditableText);

  // tell SDLApp we want to allow text input
  enableTextInput ();

  // Sprites MUST be loaded in initGame 
  // (after SDLApp:createWindow() is called)
  // They cannot be loaded in the ctor
 }


//***************************************************************************
// Destructor: ~SDL2ExampleApp
//
//***************************************************************************
SDL2ExampleApp::~SDL2ExampleApp ()
{
  delete mpcEditableText;
  delete mpcNotEditableText;
  delete mpcSpriteUsingHandleEvent;
  delete mpcSpriteWithRegisteredClickHandler;
}

//***************************************************************************
// Function:    initGame
//
// Description: Create sprites and Load icons in to sprite
//
// Parameters:  None
//
// Returned:    None
//***************************************************************************
void SDL2ExampleApp::initGame ()
{

  // Sprites MUST be loaded in initGame.
  // They cannot be loaded in the ctor

  // create a sprite
  mpcSpriteWithRegisteredClickHandler = new SDLSprite (this, 
    "Sprites\\X.png", 420, 100);

  // register a click handler to be called if the sprite is clicked
  mpcSpriteWithRegisteredClickHandler->registerClickEventHandler (
    std::bind (&SDL2ExampleApp::spriteClickHandler, this)
  );
 
  // set the sprite visible
  mpcSpriteWithRegisteredClickHandler->setVisible (true);

  // register the sprite with SDLApp so the sprite is clickable
  registerClickableWidget (mpcSpriteWithRegisteredClickHandler);


  // create a sprite
  // we will handle clicks on this sprite via
  // SDL2ExampleApp::handlEvent()
  mpcSpriteUsingHandleEvent = new SDLSprite (this, 
    "Sprites\\O.png", 300, 200);

  // set the sprite visible
  mpcSpriteUsingHandleEvent->setVisible (true);
 }

//***************************************************************************
// Function:    update
//
// Description: do nothing
//
// Parameters:  None
//
// Returned:    None
//***************************************************************************
void SDL2ExampleApp::update ()
{

}

//***************************************************************************
// Function:    handleEvent
//
// Description: Handle a mouse event. Check if a sprite was clicked
//
// Parameters:  event - SDL_Event
//
// Returned:    None
//***************************************************************************
void SDL2ExampleApp::handleEvent (SDL_Event event)
{
  int x, y;
  std::string tmpString;

  if (event.type == SDL_MOUSEBUTTONDOWN &&
    event.button.button == SDL_BUTTON_LEFT)
  {
    // where is the button click?
    x = event.button.x;
    y = event.button.y;


    // does this x,y fall inside the spirte?
    if (mpcSpriteUsingHandleEvent->clicked (x, y))
    {
      mpcNotEditableText->setData ("O Button Clicked");
    }

  }

}

//***************************************************************************
// Function:    render
//
// Description: draw all the widgets to the screen
//
// Parameters:  None
//
// Returned:    None
//***************************************************************************
void SDL2ExampleApp::render ()
{
  mpcSpriteWithRegisteredClickHandler->draw (*this);
  mpcSpriteUsingHandleEvent->draw (*this);

  mpcNotEditableText->draw (*this);
  mpcEditableText->draw (*this);
}


//***************************************************************************
// Function:    editableTextBoxStateChangeHandler
//
// Description: handle the state change for the text widget
//
// Parameters:  None
//
// Returned:    None
//***************************************************************************
void SDL2ExampleApp::editableTextBoxStateChangeHandler ()
{
  mpcNotEditableText->setData (mpcEditableText->getData ());
}

//***************************************************************************
// Function:    spriteClickHandler
//
// Description: handle the sprite click
//
// Parameters:  None
//
// Returned:    None
//***************************************************************************
void SDL2ExampleApp::spriteClickHandler ()
{
  mpcNotEditableText->setData ("X Button Clicked");
}