//****************************************************************************
// File name:	 SDL2ExampleApp.h
// Author:     Chadd Williams
// Date:       4/12/2019
// Class:      CS 485
// Assignment: MVP
// Purpose:    Demonstrate how to use SDL widgets and events.
//****************************************************************************

#pragma once

#include "SDLApp.h"
#include "SDLSprite.h"
#include "SDLTextWidget.h"

class SDL2ExampleApp : public SDLApp {

  public:

    SDL2ExampleApp ();

    ~SDL2ExampleApp ();


    virtual void initGame ();
    virtual void update ();
    virtual void handleEvent (SDL_Event event);

    virtual void render () ;


  private:


    SDLTextWidget* mpcNotEditableText;

    SDLTextWidget* mpcEditableText;
    void editableTextBoxStateChangeHandler ();


    SDLSprite* mpcSpriteWithRegisteredClickHandler;
    void spriteClickHandler ();

    SDLSprite* mpcSpriteUsingHandleEvent;

    int mConsolasFontID;
    int mVerdanaFontID;

};