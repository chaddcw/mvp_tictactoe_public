
[CS 485: Advanced Object Oriented Design](http://zeus.cs.pacificu.edu/chadd/cs485s19)

# Project Descriptions 

### 12_ObserverExample

Example of the Observer Patterns.  Uses SDL to draw sprites and text widgets on the screen.  Simple event handling.
	
### ModelViewPresenter_TicTacToe

The Model for TicTacToe.  Also contains the abstract interface for the View and Presenter as well as the actual concrete Presenter.
	
### ModelViewPresenter_TicTacToe_SDL

The SDL based View for TicTacToe.
	
### ModelViewPresenter_TicTacToe_TextUI

The TextUI based View for TicTacToe.

### SDL2_Example

Basic SDLApp2 example using sprites, text widgets, and various ways of handling events.
	
### SDLTextWidgetExample

SDLApp2 example focusing on the use of text widgets.
	

### SDLApp2

The SDL wrapper to use.
	
### TextUI

The console based UI.  This provides text widgets and allows for event driven programming.
	
	

## Branches:
- master
  - MVP is implemented in a straight forward fashion.
		
- Observer
    - The Observer pattern (from 12_ObserverExample) is used in the MVP pattern.  Parts of the Model are Observable and the View observes them.
